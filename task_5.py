def index_mass(height, weight):
    result = (weight / (height ** 2))
    print(round(result, 2))
    
    if result < 18.5:
        print('Недостатня вага')
    elif result < 24.9:
        print('Маса тіла в нормі')
    elif result > 25:
        print('Слідкуйте за фігурою')

while True:
    height = float(input('Введіть зріст: '))
    weight = float(input('Введіть вагу: '))

    index_mass(height, weight)
    stop = input('Напишіть off щоби зупинитися: ')
    
    if stop.lower() == 'off':
        break