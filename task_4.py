def arithmetic(x):
    sum_num = 0
    for num in x:
        sum_num += num
    return sum_num / 3

numbers = []

while True:
    number = int(input('Enter number: '))
    numbers.append(number)
    if len(numbers) == 3:
        print(arithmetic(numbers))
        break
