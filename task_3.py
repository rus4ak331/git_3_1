def sum_num(x1, x2):
    print(x1 + x2)

def subtraction(x1, x2):
    print(x1 - x2)

def multiplication(x1, x2):
    print(x1 * x2)

def division(x1, x2):
    if x2 == 0:
        print('Число не можна поділити на 0')
    else:
        print(x1 / x2)

def degree(x1, x2):
    print(x1 ** x2)

def root_2(x):
    print(x ** 0.5)

def root_3(x):
    print(x ** (1/3))

while True:
    print('Q - Stop')
    operation = input('Виберіть операцію(+, -, *, /, **, root): ')
    if operation.upper() == 'Q':
        break
    else:
        x1 = int(input('Введіть перше число: '))
        x2 = int(input('Введіть друге число: '))
        if operation == '+':
            sum_num(x1, x2)

        elif operation == '-':
            subtraction(x1, x2)

        elif operation == '*':
            multiplication(x1, x2)

        elif operation == '/':
            division(x1, x2)

        elif operation == '**':
            degree(x1, x2)

        elif operation.lower() == 'root':
            if x2 == 2:
                root_2(x1)
            elif x2 == 3:
                root_3(x1)
            else:
                print('В другому числі виберіть: 2(квадратний корінь) або 3(кубічний корінь)')
        
        else:
            print('Операцію не знайдено')