import math

def get_sin(value):
    return math.sin(value)

def get_root(value):
    return value ** 0.5

value = -5
while True:
    print(f'root {value} = {get_root(value)}')

    value += 0.5
    if value > 5:
        value = -5
        break

while True:
    print(f'sin {value} = {get_sin(value)}')

    value += 0.5
    if value > 5:
        break